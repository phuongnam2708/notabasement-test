import React from 'react';
import Step from './step'
import './App.scss';

function App() {
  const listSteps = [
    {
      id:'1',
      label:'Supplier'
    },
    {
      id:'2',
      label:'Basic Information'
    },
    {
      id:'3',
      label:'Language'
    },
    {
      id:'4',
      label:'Sample'
    }
  ]
  return (
    <div id="App">
      <div className="stepper">
        {listSteps && listSteps.map((item, index, arr)=><Step last={arr.length-1} index={index} item={item}/>)}
      </div>
    </div>
  );
}

export default App;
