import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import './style.scss';

const Step = (props) => {
    return (
        <div id="step-item">
            <hr className="hr-left" style={{visibility:props.index !== 0?'':'hidden'}} />
            <Container className="step">
                <Row>
                    <Col>
                        <div className="outside-border-hover">
                            <div className="step-number">
                                {props.item.id}
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row className="step-label">
                    <Col>
                        {props.item.label}
                    </Col>
                </Row>
            </Container>
            <hr className="hr-right" style={{visibility:props.index !== props.last?'':'hidden'}}/>
        </div>
    );
};

export default Step;